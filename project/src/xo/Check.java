package xo;

public class Check {
	private Pum[][] pum;

	public void setPum(Pum[][] pum) {
		this.pum = pum;
	}

	public boolean checkClickAllM() {
		if (pum[0][0].getbClick() == true && pum[0][2].getbClick() == true
				&& pum[2][0].getbClick() == true
				&& pum[2][2].getbClick() == true)
			return true;
		return false;
	}

	public boolean checkClickAllD() {
		if (pum[0][1].getbClick() == true && pum[1][0].getbClick() == true
				&& pum[1][2].getbClick() == true
				&& pum[2][1].getbClick() == true)
			return true;
		return false;
	}

	public boolean checkClickK() {
		if (pum[1][1].getbClick() == true)
			return true;
		return false;
	}

	public boolean checkTid(String n1, String n2) {
		switch (n1) {
		case "00":
			if (n2.equals("01") || n2.equals("10"))
				return true;
			break;
		case "01":
			if (n2.equals("00") || n2.equals("02"))
				return true;
			break;
		case "02":
			if (n2.equals("01") || n2.equals("12"))
				return true;
			break;
		case "10":
			if (n2.equals("00") || n2.equals("20"))
				return true;
			break;
		case "12":
			if (n2.equals("02") || n2.equals("22"))
				return true;
			break;
		case "20":
			if (n2.equals("10") || n2.equals("21"))
				return true;
			break;
		case "21":
			if (n2.equals("20") || n2.equals("22"))
				return true;
			break;
		case "22":
			if (n2.equals("21") || n2.equals("12"))
				return true;
			break;
		default:
			return false;
		}
		return false;
	}

	
	public boolean checkAcross(String n1, String n2) {
		switch (n1) {
		case "00":
			if (n2.equals("22"))
				return true;
			break;
		case "01":
			if (n2.equals("21"))
				return true;
			break;
		case "02":
			if (n2.equals("20"))
				return true;
			break;
		case "10":
			if (n2.equals("12"))
				return true;
			break;
		case "12":
			if (n2.equals("10"))
				return true;
			break;
		case "20":
			if (n2.equals("02"))
				return true;
			break;
		case "21":
			if (n2.equals("01"))
				return true;
			break;
		case "22":
			if (n2.equals("00"))
				return true;
			break;
		default:
			return false;
		}
		return false;
	}

	public String CanWin() {
		if (((pum[0][0].getTypeXO() == "O" && pum[0][1].getTypeXO() == "O")
				|| (pum[2][0].getTypeXO() == "O" && pum[1][1].getTypeXO() == "O") || (pum[2][2]
				.getTypeXO() == "O" && pum[1][2].getTypeXO() == "O"))
				&& pum[0][2].getbClick() == false)
			return "02";
		else if (((pum[0][0].getTypeXO() == "O" && pum[0][2].getTypeXO() == "O") || (pum[2][1]
				.getTypeXO() == "O" && pum[1][1].getTypeXO() == "O"))
				&& pum[0][1].getbClick() == false)
			return "01";
		else if (((pum[0][1].getTypeXO() == "O" && pum[0][2].getTypeXO() == "O")
				|| (pum[1][0].getTypeXO() == "O" && pum[2][0].getTypeXO() == "O") || (pum[2][2]
				.getTypeXO() == "O" && pum[1][1].getTypeXO() == "O"))
				&& pum[0][0].getbClick() == false)
			return "00";
		else if (((pum[0][0].getTypeXO() == "O" && pum[2][0].getTypeXO() == "O") || (pum[1][1]
				.getTypeXO() == "O" && pum[1][2].getTypeXO() == "O"))
				&& pum[1][0].getbClick() == false)
			return "10";
		else if (((pum[0][0].getTypeXO() == "O" && pum[2][2].getTypeXO() == "O")
				|| (pum[0][1].getTypeXO() == "O" && pum[2][1].getTypeXO() == "O")
				|| (pum[0][2].getTypeXO() == "O" && pum[2][0].getTypeXO() == "O") || (pum[1][0]
				.getTypeXO() == "O" && pum[1][2].getTypeXO() == "O"))
				&& pum[1][1].getbClick() == false)
			return "11";
		else if (((pum[0][2].getTypeXO() == "O" && pum[2][2].getTypeXO() == "O") || (pum[1][0]
				.getTypeXO() == "O" && pum[1][1].getTypeXO() == "O"))
				&& pum[1][2].getbClick() == false)
			return "12";
		else if (((pum[0][0].getTypeXO() == "O" && pum[1][0].getTypeXO() == "O")
				|| (pum[1][1].getTypeXO() == "O" && pum[0][2].getTypeXO() == "O") || (pum[2][1]
				.getTypeXO() == "O" && pum[2][2].getTypeXO() == "O"))
				&& pum[2][0].getbClick() == false)
			return "20";
		else if (((pum[2][0].getTypeXO() == "O" && pum[2][2].getTypeXO() == "O") || (pum[1][1]
				.getTypeXO() == "O" && pum[0][1].getTypeXO() == "O"))
				&& pum[2][1].getbClick() == false)
			return "21";
		else if (((pum[2][0].getTypeXO() == "O" && pum[2][1].getTypeXO() == "O")
				|| (pum[0][0].getTypeXO() == "O" && pum[1][1].getTypeXO() == "O") || (pum[0][2]
				.getTypeXO() == "O" && pum[1][2].getTypeXO() == "O"))
				&& pum[2][2].getbClick() == false)
			return "22";
		else
			return "";
	}

	public String CanDug() {
		if (((pum[0][0].getTypeXO() == "X" && pum[0][1].getTypeXO() == "X")
				|| (pum[2][0].getTypeXO() == "X" && pum[1][1].getTypeXO() == "X") || (pum[2][2]
				.getTypeXO() == "X" && pum[1][2].getTypeXO() == "X"))
				&& pum[0][2].getbClick() == false)
			return "02";
		else if (((pum[0][0].getTypeXO() == "X" && pum[0][2].getTypeXO() == "X") || (pum[2][1]
				.getTypeXO() == "X" && pum[1][1].getTypeXO() == "X"))
				&& pum[0][1].getbClick() == false)
			return "01";
		else if (((pum[0][1].getTypeXO() == "X" && pum[0][2].getTypeXO() == "X")
				|| (pum[1][0].getTypeXO() == "X" && pum[2][0].getTypeXO() == "X") || (pum[2][2]
				.getTypeXO() == "X" && pum[1][1].getTypeXO() == "X"))
				&& pum[0][0].getbClick() == false)
			return "00";
		else if (((pum[0][0].getTypeXO() == "X" && pum[2][0].getTypeXO() == "X") || (pum[1][1]
				.getTypeXO() == "X" && pum[1][2].getTypeXO() == "X"))
				&& pum[1][0].getbClick() == false)
			return "10";
		else if (((pum[0][0].getTypeXO() == "X" && pum[2][2].getTypeXO() == "X")
				|| (pum[0][1].getTypeXO() == "X" && pum[2][1].getTypeXO() == "X")
				|| (pum[0][2].getTypeXO() == "X" && pum[2][0].getTypeXO() == "X") || (pum[1][0]
				.getTypeXO() == "X" && pum[1][2].getTypeXO() == "X"))
				&& pum[1][1].getbClick() == false)
			return "11";
		else if (((pum[0][2].getTypeXO() == "X" && pum[2][2].getTypeXO() == "X") || (pum[1][0]
				.getTypeXO() == "X" && pum[1][1].getTypeXO() == "X"))
				&& pum[1][2].getbClick() == false)
			return "12";
		else if (((pum[0][0].getTypeXO() == "X" && pum[1][0].getTypeXO() == "X")
				|| (pum[1][1].getTypeXO() == "X" && pum[0][2].getTypeXO() == "X") || (pum[2][1]
				.getTypeXO() == "X" && pum[2][2].getTypeXO() == "X"))
				&& pum[2][0].getbClick() == false)
			return "20";
		else if (((pum[2][0].getTypeXO() == "X" && pum[2][2].getTypeXO() == "X") || (pum[1][1]
				.getTypeXO() == "X" && pum[0][1].getTypeXO() == "X"))
				&& pum[2][1].getbClick() == false)
			return "21";
		else if (((pum[2][0].getTypeXO() == "X" && pum[2][1].getTypeXO() == "X")
				|| (pum[0][0].getTypeXO() == "X" && pum[1][1].getTypeXO() == "X") || (pum[0][2]
				.getTypeXO() == "X" && pum[1][2].getTypeXO() == "X"))
				&& pum[2][2].getbClick() == false)
			return "22";
		else
			return "";
	}

}
