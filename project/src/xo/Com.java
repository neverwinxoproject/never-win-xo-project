package xo;

public class Com extends RandomPoint {

	private String[] nameRound;
	private int round;
	private int startPlayer;
	private Pum[][] pum;

	public void setNameRound(String[] nameRound) {
		this.nameRound = nameRound;
	}

	public void setStartPlayer(int startPlayer) {
		this.startPlayer = startPlayer;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public String chose(int round, String[] nameRound, int startPlayer,
			Pum[][] pum) {
		// set
		this.setNameRound(nameRound);
		this.setStartPlayer(startPlayer);
		this.setRound(round);
		this.pum = pum;
		super.setPum(pum);

		return clever();
		// return clever();

	}

	public String clever() {
		String win = CanWin();
		String dug = CanDug();
		if (win != "")
			return win;
		if (dug != "")
			return dug;

		if (startPlayer == 1)
			return comAfter();
		else if (startPlayer == 2)
			return comStart();

		return randomAll();
	}

	public String comAfter() {
		if (Pum.NameToPoint(nameRound[1]) == "K") {
			if (round == 2)
				return randomM();
			if (round == 4)
				return randomM();

		} else if (Pum.NameToPoint(nameRound[1]) == "M") {
			if (round == 2)
				return K();

			if (round == 4) {
				if (checkAcross(nameRound[1], nameRound[3]))
					return randomD();
				else
					return MTid(nameRound[3]);
			}

		} else if (Pum.NameToPoint(nameRound[1]) == "D") { // don't finish
			if (round == 2)
				return K();

			if (Pum.NameToPoint(nameRound[3]) == "M") {
				if (round == 4)
					return MTid(nameRound[1]);
			} else if (Pum.NameToPoint(nameRound[3]) == "D") {
				if (checkAcross(nameRound[1], nameRound[3])) {
					if (round == 4)
						return randomD();
					if (round == 6)
						return MTid(nameRound[4]);
				} else {
					if (round == 4)
						return MTid(nameRound[3]);
				}
			}
		}

		return randomAll();
	}

	public String comStart() {
		
		//if (round == 1)
			//return "21";
		if (Pum.NameToPoint(nameRound[1]) == "K") {
			return comStartK();
		} else if (Pum.NameToPoint(nameRound[1]) == "M") {
			return comStartM();
		} else if (Pum.NameToPoint(nameRound[1]) == "D") {
			return comStartD();
		}

		return randomAll();
	}

	public String comStartK() {
		if (Pum.NameToPoint(nameRound[2]) == "D") {
			if (round == 3)
				return MNoTid(nameRound[2]);
		} else if (Pum.NameToPoint(nameRound[2]) == "M") {
			if (round == 3)
				return MAcross(nameRound[2]);
			if (round == 5)
				return MNoTid(nameRound[4]);
		}

		return randomAll();
	}

	public String comStartM() {
		if (Pum.NameToPoint(nameRound[2]) == "D") {
			if (round == 3)
				return K();
			if (round == 5)
				return DTid(nameRound[1]);
		} else if (Pum.NameToPoint(nameRound[2]) == "M") {
			if (round == 3)
				return MNoAcross(nameRound[1]);
			if (round == 5)
				return randomM();
		} else if (Pum.NameToPoint(nameRound[2]) == "K") {
			if (round == 3) {
				int i = (int) (Math.random() * 2);
				if (i == 0)
					return MAcross(nameRound[1]);
				else
					return DNoTid(nameRound[1]);
			}
			if (round == 5) {
				if (Pum.NameToPoint(nameRound[3]) == "D") {
					String str;
					do {
						str = MTid(nameRound[3]);
					} while (checkAcross(nameRound[1], str));
					int i = Pum.getRows(str);
					int j = Pum.getCols(str);
					if (pum[i][j].getbClick() == false)
						return str;
				}

			}

		}
		return randomAll();
	}

	public String comStartD() {
		if (Pum.NameToPoint(nameRound[2]) == "D") {
			if (round == 3)
				return K();
			if (round == 5)
				if (checkAcross(nameRound[1], nameRound[2]))
					if (Pum.NameToPoint(nameRound[4]) == "D")
						return MTid(nameRound[1]);
					else
						return randomD();
				else
					return MTid(nameRound[1]);

		} else if (Pum.NameToPoint(nameRound[2]) == "M") {
			if (checkTid(nameRound[1], nameRound[2])) {
				if (round == 3) {
					return DTid(nameRound[2]);
				}
				if (round == 5)
					if (checkClickK() == false)
						return K();

			} else {
				if (round == 3) {
					String str;
					do {
						str = MTid(nameRound[1]);
					} while (checkAcross(nameRound[2], str));
					return str;
				}
			}

		} else if (Pum.NameToPoint(nameRound[2]) == "K") {
			if (round == 3)
				return DNoAcross(nameRound[1]);
			if (round == 5)
				return MTid(nameRound[1], nameRound[3]);
		}
		return randomAll();
	}

}
