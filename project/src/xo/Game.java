package xo;

import javax.swing.*;

public class Game extends JFrame {

	private Game2 game2 = new Game2();

	public Game() {
		super("GAME XO");
		add(game2);
		setFrameFeatures();
	}

	public void setFrameFeatures() {
		// set window
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
