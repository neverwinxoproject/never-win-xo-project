package xo;

import java.awt.event.*;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.*;

public class Game2 extends Table {
	final int rows = 3, cols = 3;
	private Pum[][] pum = new Pum[rows][cols];
	private int level = 0;
	private int startPlayer = 1;
	private int player = 1;
	private int round = 1;
	private String[] nameRound = new String[(rows * cols) + 1]; // round[0]
																// don't play
	private Com com;

	private int lifePlayer = 5;
	private int lifeCom = 3;

	private Listen[][] listen = new Listen[rows][cols];

	private boolean bClick = true;

	// timer for click
	private Timer timer;
	private MyTask t;
	private boolean isJubTime = false;

	// timer for click
	private Timer timer2;
	private MyTask t2;
	private boolean isJubTime2 = false;

	private boolean time = false;
	private boolean first = false;
	private boolean random = false;

	// constructor
	public Game2() {
		super();
		clear();
		showText("x play");
		changeLevel();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				listen[i][j] = new Listen(i, j);
				lbPum[i][j].addMouseListener(listen[i][j]);
				// System.out.println(com.randomD());
			}
		}

		whoPlay(player);
	}

	public void whoPlay(int player) {
		if (round == 1 && random == true) {
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++) {
					pum[i][j].setNumber();
					int number = pum[i][j].getNumber();
					showNumber(number, i, j);
				}
			bClick = false;
			timer2 = new Timer();
			MyTaskRandom t2;
			t2 = new MyTaskRandom();
			timer2.schedule(t2, 0, 1000);

		} else {
			whoPlay2(player);
		}
	}

	public void whoPlay2(int player) {
		bClick = true;
		if (player == 2) {
			comPlay();
		} else {
			playerPlay();
		}
	}

	public void comPlay() {
		// showText("O play");
		String str = com.chose(round, nameRound, startPlayer, pum);
		click(Pum.getRows(str), Pum.getCols(str));

	}

	public void playerPlay() {
		// showText("X play");
		if (first == true && round == 1) {
			click();
		}
		if (time == true) {
			stopTime();
			jubTime();
		}
	}

	public void stopTime() {
		if (isJubTime) {
			t.cancelMe();
			isJubTime = false;
		}
	}

	public void jubTime() {
		isJubTime = true;
		timer = new Timer();
		t = new MyTask();
		timer.schedule(t, 0, 1000);
	}

	public void click() { // random click when click again
		int i = (int) (Math.random() * 2);
		int j = (int) (Math.random() * 2);
		click(i, j);
	}

	public void click(int i, int j) {
		// click again
		stopTime();
		if (pum[i][j].getbClick() == true) {
			String str = com.randomAll();
			i = Pum.getRows(str);
			j = Pum.getCols(str);
		}

		// interface
		clickForShow(i, j);

	}

	public void clickForShow(int i, int j) {
		if (player == 1) {
			if (random == true) {
				int x;
				try {
					x = Integer.parseInt(JOptionPane
							.showInputDialog("ช่องนี้คือ เลขอะไร"));
				} catch (NumberFormatException e) {
					x = 0;
					// System.out.println();
				}
				if (x != pum[i][j].getNumber()) {
					i = (int) (Math.random() * 2);
					j = (int) (Math.random() * 2);
					if (pum[i][j].getbClick() == true) {
						String str = com.randomAll();
						i = Pum.getRows(str);
						j = Pum.getCols(str);
					}
				}
			}
			clickX(i, j);
		} else
			clickO(i, j);

		// inCode
		nameRound[round] = pum[i][j].getName();
		pum[i][j].click(player);
		// System.out.println(Arrays.toString(nameRound));
		clickForWin();
	}

	public void clickForWin() {
		if (winLose() == 2) {
			Sound sound = new Sound("lose.wav");
			lifePlayer--;
			showText("lifePlayer = " + lifePlayer);
			showMsg("you lose");
			nextGame();
		} else if (winLose() == 1) {
			Sound sound = new Sound("win.wav");
			lifeCom--;
			showText("lifeCom = " + lifeCom);
			showMsg("you win");
			nextGame();
		} else if (round == 9) {
			Sound sound = new Sound("draw.wav");
			lifeCom--;
			showText("lifeCom = " + lifeCom);
			showMsg("draw");
			nextGame();
		} else {
			switchPlayer();
		}
	}

	public void switchPlayer() {
		if (player == 1) {
			player = 2;
		} else {
			player = 1;
		}
		round++;
		whoPlay(player);
	}

	public void switchStartPlayer() {
		if (startPlayer == 1)
			startPlayer = 2;
		else
			startPlayer = 1;
		player = startPlayer;
		whoPlay(player);
	}

	public void nextGame() {
		clear();
		if (lifePlayer == 0) {
			Sound sound = new Sound("gameOver.wav");
			showMsg("Game over");
			System.exit(0);
		}
		if (lifeCom == 0) {
			if (level == 10) {
				Sound sound = new Sound("winLast.wav");

				showMsg("Game End");
				System.exit(0);
			}
			changeLevel();
		}
		switchStartPlayer();
	}

	public void resetLevel() {
		time = false;
		first = false;
		random = false;
		com = new Com();
	}

	public void changeLevel() {
		resetLevel();
		lifeCom = 3;
		level++;
		if (level == 1) {
			showMsg("level 1\n ระดับที่ 1\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random");
			super.changeHead("image/pp1.png");
			com = new ComEasy();

		} else if (level == 2) {
			super.changeHead("image/pp2.png");
			showMsg("level 2\n ระดับที่ 2\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random");
			com = new ComNormal();

		} else if (level == 3) {
			super.changeHead("image/pp3.png");
			showMsg("level 3\n ระดับที่ 3\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random\n มีเวลาให้คิด 3 วินาที ถ้าไม่คลิกภายในเวลาที่กำหนดระบบจะ  Random");
			time = true;
			com = new ComNormal();
		} else if (level == 4) {
			super.changeHead("image/pp4.png");
			showMsg("level 4\n ระดับที่ 4\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random\n มีเวลาให้คิด 3 วินาที ถ้าไม่คลิกภายในเวลาที่กำหนดระบบจะ  Random");
		} else if (level == 5) {
			super.changeHead("image/pp5.png");
			showMsg("level 5\n ระดับที่ 5\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random\n Xเป็นคนเริ่มเกม");
			first = true;
		} else if (level == 6) {
			super.changeHead("image/pp6.png");
			showMsg("level 6\n ระดับที่ 6\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random\n มีเวลาให้คิด 3 วินาที ถ้าไม่คลิกภายในเวลาที่กำหนดระบบจะ  Random");
			time = true;
		} else if (level == 7) {
			super.changeHead("image/pp7.png");
			showMsg("level 7\n ระดับที่ 7\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random\n ระบบจะมีการสุ่มตัวเลขในตารางให้ จำและตอบให้ถูก \n ถ้าตอบถูกสามารถลงXที่ช่องนั้นๆได้ ถ้าผิดระบบจะ Random");
			random = true;
		} else if (level == 8) {
			super.changeHead("image/pp8.png");
			showMsg("level 8\n ระดับที่ 8\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random\n เช่นเดียวกับระดับ 7 โดยXเป็นผู้เริ่ม");
			random = true;
			first = true;
		} else if (level == 9) {
			super.changeHead("image/pp9.png");
			showMsg("level 9\n ระดับที่ 9\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random\n เช่นเดียวกับระดับ 7 โดย มีเวลาให้คิด 3 วินาที ถ้าไม่คลิกภายในเวลาที่กำหนดระบบจะ  Random");
			random = true;
			time = true;
		} else if (level == 10) {
			super.changeHead("image/ppZ.png");
			showMsg("level 10\n ระดับที่  10\n ลง X เรียงกันสามตัวเพื่อชนะเมื่อคลิกที่ช่องเดิมจะเป็นการ Random\n X เป็นผู้เริ่ม  มีเวลาให้คิด 3 วินาที ถ้าไม่คลิกภายในเวลาที่กำหนดระบบจะ  Random และ ระบบจะมีการสุ่มตัวเลขในตารางให้ จำและตอบให้ถูก \n ถ้าตอบถูกสามารถลงXที่ช่องนั้นๆได้ ถ้าผิดระบบจะ Random");
			first = true;
			time = true;
			random = true;
		}
	}

	// clear
	public void clear() {
		super.clearlbPum();

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				pum[i][j] = new Pum(i, j);
				// System.out.println(pum[i][j].toString());
			}
		}
		round = 1;
		for (int i = 0; i < nameRound.length; i++) {
			nameRound[i] = "";
		}
	}

	// show number
	public void showNumber(int n, int i, int j) {
		icon = new ImageIcon("image/" + n + ".png");
		lbPum[i][j].setIcon(icon);
	}

	// clear number
	public void clearNumber() {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++) {
				icon = new ImageIcon("image/xo.png");
				lbPum[i][j].setIcon(icon);
			}
	}

	// show msg
	public void showMsg(String s) {
		JOptionPane.showMessageDialog(null, s);
	}

	// checkWinlose
	public int winLose() {
		for (int i = 0; i < 3; i++) {
			// naw non
			if (((pum[i][0].getTypeXO() == "X")
					&& (pum[i][1].getTypeXO() == "X") && (pum[i][2].getTypeXO() == "X"))
					|| ((pum[0][i].getTypeXO() == "X")
							&& (pum[1][i].getTypeXO() == "X") && (pum[2][i]
							.getTypeXO() == "X")))
				return 1;
			// naw tung
			if (((pum[i][0].getTypeXO() == "O")
					&& (pum[i][1].getTypeXO() == "O") && (pum[i][2].getTypeXO() == "O"))
					|| ((pum[0][i].getTypeXO() == "O")
							&& (pum[1][i].getTypeXO() == "O") && (pum[2][i]
							.getTypeXO() == "O")))
				return 2;
		}
		// tayang
		if ((pum[0][0].getTypeXO() == "X" && pum[1][1].getTypeXO() == "X" && pum[2][2]
				.getTypeXO() == "X")
				|| (pum[1][1].getTypeXO() == "X"
						&& pum[0][2].getTypeXO() == "X" && pum[2][0]
						.getTypeXO() == "X"))
			return 1;
		if ((pum[0][0].getTypeXO() == "O" && pum[1][1].getTypeXO() == "O" && pum[2][2]
				.getTypeXO() == "O")
				|| (pum[1][1].getTypeXO() == "O"
						&& pum[0][2].getTypeXO() == "O" && pum[2][0]
						.getTypeXO() == "O"))
			return 2;

		return 0;

	}

	// listen
	class Listen extends MouseAdapter {
		private int rows;
		private int cols;

		public Listen(int rows, int cols) {
			this.rows = rows;
			this.cols = cols;
		}

		public void mouseClicked(MouseEvent e) {
			if (bClick == true)
				click(rows, cols);
		}

		public void mouseEntered(MouseEvent e) {
			// icon = new ImageIcon("image/p02.png");
			// lbPum[rows][cols].setIcon(icon);
		}

		public void mouseExited(MouseEvent e) {
			// icon = new ImageIcon("image/p01.png");
			// lbPum[rows][cols].setIcon(icon);
		}
		/*
		 * public void actionPerformed(ActionEvent e) { click(rows, cols); }
		 */
	}

	// timer for click
	class MyTask extends TimerTask {
		// times member represent calling times.
		private int times = 3;

		public void run() {
			if (times > 0) {
				// System.out.println(times);
				showText("Time remaining " + times);
			} else {
				showText("Random click");
				click();
				// Stop Timer.
				cancelMe();
			}
			times--;
		}

		public void cancelMe() {
			this.cancel();
		}
	}

	// timer for random
	class MyTaskRandom extends TimerTask {
		// times member represent calling times.
		private int times = 5;

		public void run() {
			if (times > 0) {
				// System.out.println(times);
				showText("Time remaining for watch number" + times);
			} else {
				showText("Hind number");
				clearNumber();
				whoPlay2(player);
				// Stop Timer.
				cancelMe();
			}
			times--;
		}

		public void cancelMe() {
			this.cancel();
		}
	}

	// delay
	public void pause(int seconds) {
		/*
		 * Date start = new Date(); Date end = new Date(); while (end.getTime()
		 * - start.getTime() < seconds * 1000) { end = new Date(); }
		 */

		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
