package xo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Howtoplay extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1098252640906873258L;

	JLabel Next;
	ImageIcon icon;
	JPanel FPanel;
	JPanel SPanel;
	JLabel Back;
	JPanel TPanel;

	public Howtoplay() {
		super("How to play");
		CreateComponents();
		setFrameFeatures();
		addListener();
	}

	public void CreateComponents() {
		RImage IMG = new RImage("BG2.png");
		FPanel = new JPanel();
		SPanel = new JPanel();
		TPanel = new JPanel();
		setContentPane(FPanel);
		FPanel.setLayout(new BorderLayout());
		FPanel.add(IMG, BorderLayout.NORTH);
		icon = new ImageIcon("image/P01.png");
		Next = new JLabel(icon, SwingConstants.CENTER);
		SPanel.setLayout(new BorderLayout());
		SPanel.add(Next, BorderLayout.CENTER);
		FPanel.add(SPanel, BorderLayout.SOUTH);
	}

	public void addListener() {
		Next.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				Game game = new Game();
			}

			public void mouseEntered(MouseEvent e) {
				icon = new ImageIcon("image/p02.png");
				Next.setIcon(icon);

			}

			public void mouseExited(MouseEvent e) {
				icon = new ImageIcon("image/p01.png");
				Next.setIcon(icon);
			}
		});
	}

	public void setFrameFeatures() {
		// set window
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}

class RImage extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4264343439588585903L;
	BufferedImage img;

	public void paint(Graphics g) {
		g.drawImage(img, 0, 0, null);

	}

	public RImage(String image) {
		try {
			img = ImageIO.read(new File("image/" + image));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Dimension getPreferredSize() {
		if (img == null) {
			return new Dimension(100, 100);
		} else {
			return new Dimension(img.getWidth(), img.getHeight());
		}
	}

}