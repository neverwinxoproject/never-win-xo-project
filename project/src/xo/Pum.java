package xo;

public class Pum {

	private String name;
	private boolean bClick;
	private String typeXO;
	private String point;
	private int number;

	public Pum(int i, int j) {
		String str = "" + i + j;
		this.setName(str);
		this.setbClick(false);
		this.setPoint(NameToPoint(str));
		this.setTypeXO("");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getbClick() {
		return bClick;
	}

	public void setbClick(boolean bClick) {
		this.bClick = bClick;
	}

	public String getTypeXO() {
		return typeXO;
	}

	public void setTypeXO(String typeXO) {
		this.typeXO = typeXO;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public void click(int player) {
		this.setbClick(true);
		if (player == 1)
			this.typeXO = "X";
		else
			this.typeXO = "O";
	}

	// get number
	public int getNumber() {
		return number;
	}

	// set number
	public void setNumber() {
		number = (int) ((Math.random() * 9) + 1);
	}

	// method static

	public static String NameToPoint(String name) {
		if (name == "")
			return "";
		int i = getRows(name);
		int j = getCols(name);
		if (i == 1 && j == 1)
			return "K";
		else if ((i + j) % 2 == 0)
			return "M";
		else
			return "D";
	}

	public static int getRows(String name) {
		int i = Integer.parseInt(name.charAt(0) + "");
		return i;
	}

	public static int getCols(String name) {
		int j = Integer.parseInt(name.charAt(1) + "");
		return j;
	}

	public static String intToString(int n) {
		if (n == 0)
			return "0";
		else if (n == 1)
			return "1";
		else
			return "2";
	}

	public String toString() {
		return "name = " + this.getName() + " click = " + this.getbClick()
				+ " XO = " + this.getTypeXO() + " point = " + this.getPoint();
	}
}
