package xo;

public class RandomPoint extends Check {

	private int i;
	private int j;
	private Pum[][] pum;

	public void setPum(Pum[][] pum) {
		this.pum = pum;
		super.setPum(pum);
	}

	public String randomM() {
		do {
			// System.out.println("M");
			i = (int) (Math.random() * 2);
			j = (int) (Math.random() * 2);
			if (i == 1)
				i = 2;
			if (j == 1)
				j = 2;
		} while (pum[i][j].getbClick() == true);
		return pum[i][j].getName();

	}

	public String randomD() {
		do {
			// System.out.println("D");
			i = (int) (Math.random() * 2);
			j = (int) (Math.random() * 2);
			if (i + j == 0) {
				i = 1;
				j = 2;
			}
			if (i + j == 2) {
				i = 2;
				j = 1;
			}
		} while (pum[i][j].getbClick() == true);
		return pum[i][j].getName();
	}

	public String K() {
		return "11";
	}

	public String randomAll() {
		do {
			int NumRandom = (int) (Math.random() * 3);
			//System.out.println(NumRandom);
			if (NumRandom == 0) {
				if (checkClickAllM() == false)
					return randomM();
				else
					continue;
			}
			if (NumRandom == 1) {
				if (checkClickAllD() == false)
					return randomD();
				else
					continue;
			}
			if (NumRandom == 2) {
				if (checkClickK() == false)
					return K();
				else
					continue;
			}
		} while (true);
	}

	// Tid Across

	public String MTid(String with) {
		String str;
		do {
			str = randomM();
		} while (!checkTid(str, with));
		return str;
	}

	public String MAcross(String with) {
		String str;
		do {
			str = randomM();
		} while (!checkAcross(str, with));
		return str;
	}

	public String DTid(String with) {
		String str;
		do {
			str = randomD();

		} while (!checkTid(str, with));
		return str;
	}

	public String DAcross(String with) {
		String str;
		do {
			str = randomD();
		} while (!checkAcross(str, with));
		return str;
	}

	// No Tid Across

	public String MNoTid(String with) {
		String str;
		do {
			str = randomM();
		} while (checkTid(str, with));
		return str;
	}

	public String MNoAcross(String with) {
		String str;
		do {
			str = randomM();
		} while (checkAcross(str, with));
		return str;
	}

	public String DNoTid(String with) {
		String str;
		do {
			str = randomD();
		} while (checkTid(str, with));
		return str;
	}

	public String DNoAcross(String with) {
		String str;
		do {
			str = randomD();
		} while (checkAcross(str, with));
		return str;
	}

	// Tid double

	public String MTid(String with1, String with2) {
		String str;
		do {
			str = randomM();
		} while (!checkTid(with1, str) || !checkTid(with2, str));
		return str;
	}

}
