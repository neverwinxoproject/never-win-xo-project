package xo;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.*;

public class Table extends JPanel {

	private final int rows = 3;
	private final int cols = 3;
	protected JLabel[][] lbPum;
	protected JLabel lb, lbHead;
	protected JPanel panelTop, panelCenter, panelLb, panelTarang;
	protected ImageIcon icon;

	public void changeHead(String str) {
		/*
		 * try { panelTop.remove(lbHead); } catch (Exception e) {
		 * System.err.println(e); } icon = new ImageIcon(str); lbHead = new
		 * JLabel(icon);
		 */
		icon = new ImageIcon(str);
		lbHead.setIcon(icon);
	}

	public void createComponents() {

		panelTop = new JPanel();
		panelCenter = new JPanel();
		panelLb = new JPanel();

		setLayout(new BorderLayout());
		add(panelTop);
		add(panelCenter);
		add(panelLb);

		// panel top
		lbHead = new JLabel();
		changeHead("image/pp1.png");
		panelTop.add(lbHead);

		// panelTarang
		panelTarang = new JPanel(new GridLayout(3, 3));
		icon = new ImageIcon("image/krob.png");
		lbPum = new JLabel[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				lbPum[i][j] = new JLabel(icon);
				// lbPum[i][j].setPreferredSize(new Dimension(50, 50));
				panelTarang.add(lbPum[i][j]);
				// listen[i][j] = new Listen(i, j);
				// lbPum[i][j].addActionListener(listen[i][j]);
			}
		}

		// panelLb
		panelLb = new JPanel();
		lb = new JLabel("");
		panelLb.add(lb);

		// panel center
		panelCenter.setLayout(new BorderLayout());
		panelCenter.add(panelLb, BorderLayout.NORTH);
		panelCenter.add(panelTarang, BorderLayout.CENTER);

		// Frame
		setLayout(new BorderLayout());
		add(panelTop, BorderLayout.NORTH);
		add(panelCenter, BorderLayout.CENTER);

	}

	public void setFrameFeatures() {
		// set window
	}

	public Table() {
		createComponents();
	}

	public void clickX(int i, int j) {
		icon = new ImageIcon("image/x.png");
		lbPum[i][j].setIcon(icon);
		Sound sound = new Sound("clickX.wav");
		// lbPum[i][j].setEnabled(false);
		// lbPum[i][j].setLabel("X");
	}

	public void clickO(int i, int j) {
		Sound sound = new Sound("clickO.wav");
		icon = new ImageIcon("image/o.png");
		lbPum[i][j].setIcon(icon);
		// lbPum[i][j].setEnabled(false);
		// lbPum[i][j].setLabel("O");
	}

	public void clearlbPum() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				icon = new ImageIcon("image/xo.png");
				lbPum[i][j].setIcon(icon);
				// lbPum[i][j].setEnabled(true);
				// lbPum[i][j].setLabel("");
			}
		}
	}

	// show text
	public void showText(String s) {
		Font txt = new Font("Serif", Font.ITALIC + Font.BOLD, 24);
		lb.setFont(txt);
		lb.setForeground(Color.BLACK);
		lb.setBackground(Color.yellow);
		lb.setOpaque(true);
		lb.setText(s);
	}

	public void pause(int seconds) {
		Date start = new Date();
		Date end = new Date();
		while (end.getTime() - start.getTime() < seconds * 1000) {
			end = new Date();
		}
		/*
		 * try { Thread.sleep(1000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 */
	}

}
