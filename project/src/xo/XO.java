package xo;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class XO extends JFrame {

	protected JPanel centerPanel;
	protected JLabel playLb, exitLb;
	protected ImageIcon icon;
	protected Howtoplay howToPlay;
	protected Sound soundOpen;

	public void CreateComponents() {
		setContentPane(new JLabel(new ImageIcon(this.getClass().getResource(
				"/BG.png"))));
		icon = new ImageIcon("image/e01.png");
		exitLb = new JLabel(icon, SwingConstants.CENTER);
		icon = new ImageIcon("image/P01.png");
		playLb = new JLabel(icon, SwingConstants.CENTER);

		centerPanel = new JPanel();
		setLayout(new GridLayout(2, 1));
		add(playLb);
		add(exitLb);
		// sound
		soundOpen = new Sound("gmg.wav");

		// add(centerPanel);
	}

	public void addListener() {

		// play
		playLb.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Sound sound = new Sound("click.wav");
				setVisible(false);
				// Game game = new Game();
				howToPlay = new Howtoplay();
				soundOpen.stop();
			}

			public void mouseEntered(MouseEvent e) {
				icon = new ImageIcon("image/p02.png");
				playLb.setIcon(icon);
				Sound sound = new Sound("mouseEntered.wav");
			}

			public void mouseExited(MouseEvent e) {
				icon = new ImageIcon("image/p01.png");
				playLb.setIcon(icon);
			}
		});

		// exit
		exitLb.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Sound sound = new Sound("click.wav");
				System.exit(0);
			}

			public void mouseEntered(MouseEvent e) {
				icon = new ImageIcon("image/e02.png");
				exitLb.setIcon(icon);
				Sound mouseEntered = new Sound("mouseEntered.wav");
			}

			public void mouseExited(MouseEvent e) {
				icon = new ImageIcon("image/e01.png");
				exitLb.setIcon(icon);
			}
		});
	}

	public void setFrameFeatures() {
		// set window
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private static void createAndShowGUI() {
		XO xo = new XO();
		xo.CreateComponents();
		xo.setFrameFeatures();
		xo.addListener();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}